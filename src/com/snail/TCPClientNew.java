package com.snail;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.logging.Logger;

/**
 * Created by artfable on 20.05.14.
 * Singleton
 */
public class TCPClientNew extends Thread {

    private static Logger log = Logger.getLogger(TCPClientNew.class.getCanonicalName());

    private static final TCPClientNew INSTANCE = new TCPClientNew();

    private Socket socket;
    private PrintWriter bufferOut;
    private BufferedReader bufferIn;

    private boolean runFlag;
    private String message = "";

    private String serverAddress = "5.19.210.233";
    private int serverPort = 3000;

    private TCPClientNew() {
        super();
    }

    public static TCPClientNew getInstance() {
        return INSTANCE;
    }

    public void sendMessage(String message) {
            bufferOut.println(message);
            bufferOut.flush();
    }

    /**
     * if no message - return empty string, NOT NULL
     * @return
     */
    public String receiveMessage() {
        try {
            //костыль чтоб успел принять ответ
            TCPClientNew.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        String bufferMessage = message;
        message = "";
        return bufferMessage;
    }

    @Override
    public void run() {
        super.run();
        runFlag = true;
        try {
            socket = new Socket(serverAddress, serverPort);
            bufferOut = new PrintWriter(socket.getOutputStream());
            bufferIn = new BufferedReader(new InputStreamReader(socket.getInputStream()));

            String serverMessage;
            while (runFlag) {
                serverMessage = bufferIn.readLine();
                if (serverMessage != null) {
                    if(!message.equals("")) {
                        message += "\n";
                    }
                    message += serverMessage;
                }
            }
        } catch (IOException e) {
            log.severe("Атас!! Все сломалось!!");
            e.printStackTrace();
        }
        log.info("Connection done to " + serverAddress + ":" + serverPort);
    }

    /**
     * Всегда должен вызываться при закрытии программы. Иначе чей-то кривой сервер рискует упасть
     */
    public void closeConnection() {
        runFlag = false;
        try {
            socket.close();
        } catch (IOException e) {
            log.warning("Connection will be closed not right!!");
            e.printStackTrace();
        }
        socket = null;
        bufferIn = null;
        bufferOut = null;
        message = "";
    }
}
