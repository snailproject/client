package com.snail.contacts;

/**
 * Created by dashok on 5/25/14.
 */
public class ContactBean {
    //Properties
    private String nickname;
    private int id;
    //private boolean online;
    //private String status;
    //private String info;

    public ContactBean() {
        nickname = "";
        id = -1;
    }

    public ContactBean(String nickname, int id) {
        this.nickname = nickname;
        this.id = id;
    }

    //Getters
    public String getNickname() { return nickname; }
    public int getId() { return id; }
    //public boolean isOnline() { return online; }

    //Setters
    public void setNickname(String nickname) { this.nickname = nickname; }
    public void setId(int id) { this.id = id; }
    //public void setOnline(boolean online) { this.online = online; }
}
