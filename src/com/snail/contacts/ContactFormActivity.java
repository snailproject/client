package com.snail.contacts;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.SectionIndexer;
import android.widget.Toast;
import com.snail.R;
import com.snail.TCPClientNew;
import com.snail.msgForm.logic.MessageUtils;
import com.snail.msgForm.view.MsgFormActivity;
import org.json.JSONException;
import org.json.JSONArray;


import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by dashok on 5/21/14.
 */
public class ContactFormActivity extends Activity {
    private ArrayList<String> mContacts;
    private ArrayList<ContactBean> mContactList;
    private IndexableListView mListView;
    private boolean isInitialized = false;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.contacts_form);

        ActionBar actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        mContactList = new ArrayList<ContactBean>();

        TCPClientNew tcpClient = TCPClientNew.getInstance();
        MessageUtils messageUtils = MessageUtils.getInstance();
        tcpClient.sendMessage("getContacts " + messageUtils.getMyId());
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        String received = messageUtils.getContactsMessages();
        System.out.println("any " + received);
        if(!received.startsWith("ContactsJSON_")) {
            new AlertDialog.Builder(this)
                    .setTitle("Internal error!")
                    .setMessage("Error while loading contact list.")
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            isInitialized = false;
                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
        }
        else isInitialized = true;
        if(isInitialized) {
            received = received.replace("ContactsJSON_", "");
            try {
                JSONArray jsonContacts = new JSONArray(received);
                for(int i = 0; i < jsonContacts.length(); i++) {
                    mContactList.add(new ContactBean(jsonContacts.getJSONObject(i).getString("nickname"),
                            jsonContacts.getJSONObject(i).getInt("id")));
                }
            }
            catch (JSONException e) {
                throw new RuntimeException(e);
            }
        }
//        mContactList.add(new ContactBean("Михаил Боярский", 0));
//        mContactList.add(new ContactBean("Константин Константинович", 1));
//        mContactList.add(new ContactBean("Подлый смурф", 2));
//        mContactList.add(new ContactBean("123йцу", 3));
//        mContactList.add(new ContactBean("Акакий", 4));
//        mContactList.add(new ContactBean("Вольдемар", 5));
//        mContactList.add(new ContactBean("Эйнштейн", 6));

        mContacts = new ArrayList<String>();
//        mContacts.add("Михаил Боярский");
//        mContacts.add("Константин Константинович");
//        mContacts.add("Подлый смурф");
//        mContacts.add("123йцу");
//        mContacts.add("Акакий");
//        mContacts.add("Вольдемар");
//        mContacts.add("Эйнштейн");
//        mContacts.add("Мама");
//        mContacts.add("Автосервис");
//        mContacts.add("Лев Абалкин");
//        mContacts.add("Кончита");
//        mContacts.add("Иоганн Гутенберг");
//        mContacts.add("Яков Тыков");
//        mContacts.add("Йолкин йож");
//        mContacts.add("Мак Сим Каммерер");
//        mContacts.add("Жюль Верн");
//        mContacts.add("Зигмунд");
//        mContacts.add("Картошка с фрикадельками");
//        mContacts.add("НАЧАЛЬНИК");
//        mContacts.add("Хелена Бонем Картер");
        for(ContactBean a:mContactList) {
            mContacts.add(a.getNickname());
        }
        Collections.sort(mContacts);

        ContentAdapter adapter = new ContentAdapter(this,
                android.R.layout.simple_list_item_1, mContacts);

        mListView = (IndexableListView) findViewById(R.id.listview);
        mListView.setAdapter(adapter);
        mListView.setFastScrollEnabled(true);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                Toast.makeText(getApplicationContext(),
                        "Click ListItem Number " + position, Toast.LENGTH_LONG)
                        .show();
                Intent intent = new Intent(ContactFormActivity.this, MsgFormActivity.class);
                int talkToID = 0;
                for(ContactBean a:mContactList) {
                    if(a.getNickname() == mContacts.get(position)) {
                        talkToID = a.getId();
                        break;
                    }
                }
                intent.putExtra("talkToID", talkToID);
                startActivity(intent);
            }
        });
    }

    private class ContentAdapter extends ArrayAdapter<String> implements SectionIndexer {

        //private String mSections = "#ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        private String mSections = "#АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ";

        public ContentAdapter(Context context, int textViewResourceId,
                              List<String> objects) {
            super(context, textViewResourceId, objects);
        }

        @Override
        public int getPositionForSection(int section) {
            // If there is no item for current section, previous section will be selected
            for (int i = section; i >= 0; i--) {
                for (int j = 0; j < getCount(); j++) {
                    if (i == 0) {
                        // For numeric section
                        for (int k = 0; k <= 9; k++) {
                            if (StringMatcher.match(String.valueOf(getItem(j).charAt(0)), String.valueOf(k)))
                                return j;
                        }
                    } else {
                        if (StringMatcher.match(String.valueOf(getItem(j).charAt(0)), String.valueOf(mSections.charAt(i))))
                            return j;
                    }
                }
            }
            return 0;
        }

        @Override
        public int getSectionForPosition(int position) {
            return 0;
        }

        @Override
        public Object[] getSections() {
            String[] sections = new String[mSections.length()];
            for (int i = 0; i < mSections.length(); i++)
                sections[i] = String.valueOf(mSections.charAt(i));
            return sections;
        }
    }

    public void setContactList(ArrayList<String> contacts) {
        mContacts = contacts;
        ContentAdapter adapter = new ContentAdapter(this,
                android.R.layout.simple_list_item_1, mContacts);
        mListView.setAdapter(adapter);
    }

}
