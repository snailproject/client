package com.snail.authorization;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import com.snail.R;

/**
 * Created by gleb on 23.04.14.
 */
public class Registration extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registration_form);
    }

    public void onEnterRegistration(View v){


        EditText password = (EditText) findViewById(R.id.passwordreg);
        EditText password1 = (EditText) findViewById(R.id.password1reg);

        if(password.getText().toString().equals(password1.getText().toString())){
            Intent intent = new Intent(Registration.this,Entry.class);
            EditText login = (EditText) findViewById(R.id.loginreg);
            EditText phone = (EditText) findViewById(R.id.telephone);

            SendToServer registration = new SendToServer();

            if(registration.sendToServer(phone.getText().toString(), password.getText().toString(), login.getText().toString()))
            {
                intent.putExtra("phone",phone.getText().toString());
                intent.putExtra("password",password.getText().toString());
                startActivity(intent);
            }
            else {
                TextView log = (TextView) findViewById(R.id.logReg);
                log.setText("Error");
            }

        }else {
            TextView log = (TextView) findViewById(R.id.logReg);
            log.setText("Password != Password1");
        }

    }
}
