package com.snail.authorization;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import com.snail.R;
import com.snail.contacts.ContactFormActivity;
import com.snail.msgForm.view.MsgFormActivity;

/**
 * Created by gleb on 23.04.14.
 */
public class Entry extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.entry_form);
        if(!getIntent().getStringExtra("phone").equals("null")){
            enterAfterReg(getIntent().getStringExtra("phone"),getIntent().getStringExtra("password"));
        }
    }

    public void onEnterEntry(View v){

        TextView log = (TextView) findViewById(R.id.logEntry);

        EditText phone = (EditText) findViewById(R.id.phone);
        EditText password = (EditText) findViewById(R.id.password);
        Intent intent = new Intent(Entry.this, ContactFormActivity.class);

        log.setText("");
        SendToServer message = new SendToServer();
        if(message.sendToServer(phone.getText().toString(), password.getText().toString())){
            intent.putExtra("phone", phone.getText().toString());
            startActivity(intent);
        }
        else {
            log.setText("Invalid password");
        }
    }

    public void enterAfterReg(String phone, String password){
        SendToServer message = new SendToServer();
        Intent intent = new Intent(Entry.this, ContactFormActivity.class);

        if(message.sendToServer(phone, password)){
            intent.putExtra("phone", phone);
            startActivity(intent);
        }
    }

}
