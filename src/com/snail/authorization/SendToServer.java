package com.snail.authorization;

import com.snail.TCPClientNew;
import com.snail.msgForm.logic.MessageUtils;

import javax.crypto.*;
import javax.crypto.spec.SecretKeySpec;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

/**
 * Created by gleb on 21.05.14.
 */
public class SendToServer {

    private TCPClientNew client = TCPClientNew.getInstance();
    private MessageUtils messageUtils = MessageUtils.getInstance();

    public SendToServer(){

    }
    
    private byte[] encrypt(String password) {
        byte[] encryptPassword = null;
        byte[] salt = {'1','2','a','b','3','4','c','d'};
        try {

            byte[] key = new byte[32];
            for (int i=0; i<32; i++){
                key[i] = 0x00;
            }
            SecretKey secretKey = new SecretKeySpec(key,"AES");


            //Encrypt

            Cipher cipher = Cipher.getInstance("AES");
            cipher.init(Cipher.ENCRYPT_MODE,secretKey);
            encryptPassword = cipher.doFinal(password.getBytes());
            byte[] tmp = new byte[encryptPassword.length+salt.length];
            for(int i=0; i<tmp.length; i++){
                if(i < encryptPassword.length){
                    tmp[i] = encryptPassword[i];
                }else{
                    tmp[i] = salt[i - encryptPassword.length];
                }
            }
            encryptPassword = cipher.doFinal(tmp);

        } catch (NoSuchAlgorithmException
                | NoSuchPaddingException
                | InvalidKeyException
                | BadPaddingException
                | IllegalBlockSizeException e) {
            e.printStackTrace();
        }
        return encryptPassword;
    }

    public boolean sendToServer(String phone, String password){
        byte[] encryptPassword = encrypt(password);

//        client.sendMessage("login " + phone + " " + new String(encryptPassword));
        client.sendMessage("login " + phone + " " + password);

        String receiveMessage = client.receiveMessage();
        if(receiveMessage.startsWith("Auth")){
            messageUtils.setMyId(Integer.parseInt(receiveMessage.substring(5,receiveMessage.indexOf('.'))));
            System.out.println("auth" + messageUtils.getMyId());
            messageUtils.startReceiveMessages();
            return  true;
        }
        return false;
    }

    public boolean sendToServer(String phone, String password, String nick){
        byte[] encryptPassword = encrypt(password);

//        client.sendMessage("register " + phone + " " + new String(encryptPassword) + " " + nick);
        client.sendMessage("register " + phone + " " + password + " " + nick);

        return client.receiveMessage().equals("Reg_done.");
    }
}
