package com.snail.authorization;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import com.snail.R;
import com.snail.TCPClientNew;

/**
 * Created by gleb on 23.04.14.
 */
public class AuthorizationFormActivity extends Activity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.authorization_form);

        TCPClientNew client = TCPClientNew.getInstance();


        client.start();
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void onEntery(View v){
        Intent intent = new Intent(AuthorizationFormActivity.this, Entry.class);
        intent.putExtra("phone","null");
        startActivity(intent);
    }

    public void onRegistr(View v){
        Intent intent = new Intent(AuthorizationFormActivity.this, Registration.class);
        startActivity(intent);
    }
}
