package com.snail.authorization;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import com.snail.R;
import com.snail.settings.SettingsFormActivity;

/**
 * Created by gleb on 23.04.14.
 */
public class WorkApp extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.app_form);
        initLayout();
    }

    public void initLayout(){
        String phone = getIntent().getStringExtra("phone");
        TextView textView = (TextView) findViewById(R.id.enterField);
            textView.setText("Hello, "+phone+"!");
    }

    public void clickedSettings(View v){
        Intent intent = new Intent(WorkApp.this,SettingsFormActivity.class);
        startActivity(intent);
    }
}
