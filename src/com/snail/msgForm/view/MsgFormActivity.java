package com.snail.msgForm.view;

import android.app.ActionBar;
import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import com.snail.R;
import com.snail.msgForm.logic.Message;
import com.snail.msgForm.logic.MessageUtils;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Logger;

/**
 * Created by dashok on 4/16/14.
 */
public class MsgFormActivity extends Activity {

    private static Logger log = Logger.getLogger(MsgFormActivity.class.getCanonicalName());

    private static MessageUtils messageUtils = MessageUtils.getInstance();

    private int interlocutorId;

    private Timer timer;
    private TextView textView;
    private int messageCount = 0;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.msg_form);

        ActionBar actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        textView = (TextView)findViewById(R.id.chatView);

        //нужно передать id
        interlocutorId = getIntent().getIntExtra("talkToID", 0);
//        interlocutorId = 14;
        log.info("interlocutorId = " + interlocutorId);
        timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
//                log.info("timer start");
                List<Message> messages = messageUtils.getMessageFromUser(interlocutorId);
                if (messages.size() > messageCount) {
                    String text = messages.get(messageCount).getText();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            textView.append("\n" + text);
                        }
                    });
                    messageCount++;
                }
            }
        }, 0, 500);

    }

    public void onSendClicked(View v) {
        log.info("click on send");
        EditText msgLine = (EditText)findViewById(R.id.editText);
        String msg = msgLine.getText().toString();
        textView.append("\n" + msg);
        messageUtils.sendMessage(msg, interlocutorId);
    }

    @Override
    protected void onStop() {
        super.onStop();
        timer.cancel();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        timer.cancel();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.msgform_activity_actions, menu);
        return super.onCreateOptionsMenu(menu);
        //return true;
    }
}