package com.snail.msgForm.logic;

import com.snail.TCPClientNew;

import java.util.*;
import java.util.logging.Logger;

/**
 * Класс для работы с сообщениями, через него осуществляется прием и отправка
 *
 * Created by artfable on 26.05.14.
 */
public class MessageUtils {

    private static Logger log = Logger.getLogger(MessageUtils.class.getCanonicalName());

    private int myId;

    private static final MessageUtils INSTANCE = new MessageUtils();
    private TCPClientNew tcpClient = TCPClientNew.getInstance();
    private MessagesReceiver receiver = MessagesReceiver.getInstance();

    private Map<Integer, List<Message>> receivedMessages = new HashMap<>();

    private String contactsMessages = "";

    /**
     * Будет ли реализовано - под вопросом
     */
    private Map<Integer, List<Message>> conversations = new HashMap<>();

    private MessageUtils() {

    }

    public static MessageUtils getInstance() {
        return INSTANCE;
    }

    public int getMyId() {
        return myId;
    }

    public void setMyId(int myId) {
        this.myId = myId;
    }

    /**
     * Запускает {@link com.snail.msgForm.logic.MessagesReceiver} как демона
     * @return
     */
    public boolean startReceiveMessages() {
        try {
            receiver.setDaemon(true);
            receiver.start();
            return true;
        } catch (IllegalThreadStateException e) {
            log.severe("receiver already start");
            e.printStackTrace();
        }
        return false;
    }

    public List<Message> getReceivedMessages(int fromId) {
        return receivedMessages.get(fromId);
    }

    private boolean sendMessage(Message message) {
        String command = "message " + message.getFromId() + " " + message.getToId() + " " + message.getText();
        try {
            tcpClient.sendMessage(command);
            return true;
        } catch (Exception e) {
            log.severe("message not send");
            e.printStackTrace();
        }
        return false;
    }

    public boolean sendMessage(String text, int toId) {
        Message message = new Message(myId, toId, text, new Date());
        return sendMessage(message);
    }

    public void setMessageFromUser(int fromId, Message message) {
        if (receivedMessages.containsKey(fromId)) {
            receivedMessages.get(fromId).add(message);
            return;
        }
        ArrayList<Message> array = new ArrayList<>();
        array.add(message);
        receivedMessages.put(fromId, array);
    }

    public List<Message> getMessageFromUser(int fromId) {
        List<Message> messages = receivedMessages.get(fromId);
        return (messages == null) ? new ArrayList<Message>() : messages;
    }

    public String getContactsMessages() {
        return contactsMessages;
    }

    public void setContactsMessages(String contactsMessages) {
        this.contactsMessages = contactsMessages;
    }
}
