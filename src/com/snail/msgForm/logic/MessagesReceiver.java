package com.snail.msgForm.logic;

import com.snail.TCPClientNew;

import java.util.Date;
import java.util.StringTokenizer;
import java.util.logging.Logger;

/**
 * Принимает сообщения от сервера и сохраняет их в {@link com.snail.msgForm.logic.MessageUtils}.
 * Использовать напрямую нельзя.
 *
 * Created by artfable on 26.05.14.
 */
public class MessagesReceiver extends Thread {

    private static Logger log = Logger.getLogger(MessagesReceiver.class.getCanonicalName());

    protected static final MessagesReceiver INSTANCE = new MessagesReceiver();

    private TCPClientNew tcpClient = TCPClientNew.getInstance();
    private MessageUtils messageUtils = MessageUtils.getInstance();
    private boolean runFlag = false;

    private MessagesReceiver() {
        super();
    }

    public static MessagesReceiver getInstance() {
        return INSTANCE;
    }

    @Override
    public void run() {
        super.run();
        runFlag = true;
        while (runFlag) {
            String serverMessage = tcpClient.receiveMessage();
            if (!"".equals(serverMessage)) log.info(serverMessage);
            if (messageUtils == null) {
                messageUtils = MessageUtils.getInstance();
            }
            if (serverMessage.startsWith("Contacts")) {
                System.out.println(messageUtils);
                messageUtils.setContactsMessages(serverMessage);
            }
            if (serverMessage.startsWith("message")) {
                StringTokenizer stringTokenizer = new StringTokenizer(serverMessage, " ");
                stringTokenizer.nextToken();
                int fromId = Integer.parseInt(stringTokenizer.nextToken());
                int toId = Integer.parseInt(stringTokenizer.nextToken());
                String text = stringTokenizer.nextToken("\n");
                Message message = new Message(fromId, toId, text, new Date(), null);
                messageUtils.setMessageFromUser(fromId, message);
            }
        }
    }

    /**
     * В общем-то не нужен, так как процесс все равно демон
     */
    public void stopReceiver() {
        runFlag = false;
    }
}
