package com.snail.msgForm.logic;



import java.util.Date;
import java.util.logging.Logger;

/**
 * На данный момент необходимости в классе нет, сделан с учетом преспективы развития))
 *
 * Created by artfable on 26.05.14.
 */
public class Message {

    private static Logger log = Logger.getLogger(Message.class.getCanonicalName());

    /**
     * если отсутствует - сообщение от клиента
     */
    private int fromId;

    private int toId;
    private String text;

    /**
     * 16 android sdk не знает java.time а joda time мне ставить лень
     */
    private Date createDate;
    private Date receiveDate;

    public Message(int fromId, int toId, String text, Date createDate) {
        this.fromId = fromId;
        this.toId = toId;
        this.text = text;
        this.createDate = createDate;
    }

    /**
     * При приеме сообщения, вызывается из {@link com.snail.msgForm.logic.MessagesReceiver}
     * @param toId
     * @param text
     * @param receiveDate
     * @param createDate
     */
    protected Message(int fromId, int toId, String text, Date receiveDate, Date createDate) {
        this.fromId = fromId;
        this.toId = toId;
        this.text = text;
        this.receiveDate = receiveDate;
        this.createDate = createDate;
    }

    public int getFromId() {
        return fromId;
    }

    public int getToId() {
        return toId;
    }

    public void setToId(int toId) {
        this.toId = toId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getCreateDate() {
        return createDate;
    }
}
