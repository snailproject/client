package com.snail.settings;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import com.snail.R;

import java.io.FileOutputStream;
import java.util.Properties;

/**
 * Created by gleb on 23.04.14.
 */
public class SettingsFormActivity extends Activity {
    private Properties properties = new Properties();
    private FileOutputStream outputStream;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_form);
    }

    public void clickedSort(View view){
        switch (view.getId()){
            case R.id.nameRB:
            {
                properties.setProperty("sort","name");
                break;
            }
            case R.id.sernameRB:
            {
                properties.setProperty("sort","surname");
                break;
            }
            default: break;
        }
        try{
            outputStream = new FileOutputStream("conf.properties");
            properties.store(outputStream,null);
        }catch (Exception e){
            System.out.print(e.toString());
        }
    }
    public void clickedBirthday(View view){
        CheckBox birthday = (CheckBox) findViewById(R.id.birthday);
        if (birthday.isChecked()){
            properties.setProperty("birthday", "true");
        }
        else{
            properties.setProperty("birthday", "false");
        }
        try{
            outputStream = new FileOutputStream("conf.properties");
            properties.store(outputStream,null);
        }catch (Exception e){
            System.out.print(e.toString());
        }
    }

    public void clickedRing(View view){
        switch (view.getId()){
            case R.id.ring1:
            {
                properties.setProperty("ring", "1");
                break;
            }
            case R.id.ring2:
            {
                properties.setProperty("ring", "2");
                break;
            }
            case R.id.ring3:
            {
                properties.setProperty("ring", "3");
                break;
            }
            default: break;
        }
        try{
            outputStream = new FileOutputStream("conf.properties");
            properties.store(outputStream,null);
        }catch (Exception e){
            System.out.print(e.toString());
        }
    }

    public void clickedVibro(View view){
        CheckBox vibro = (CheckBox) findViewById(R.id.vibration);
        if (vibro.isChecked()){
            properties.setProperty("vibration", "true");
        }
        else{
            properties.setProperty("vibration", "false");
        }
        try{
            outputStream = new FileOutputStream("conf.properties");
            properties.store(outputStream,null);
        }catch (Exception e){
            System.out.print(e.toString());
        }
    }

    public void clickedSave(View view){
        EditText nickname = (EditText) findViewById(R.id.nickname);
        properties.setProperty("nickname", nickname.getText().toString());
        try{
            outputStream = new FileOutputStream("conf.properties");
            properties.store(outputStream,null);
        }catch (Exception e){
            System.out.print(e.toString());
        }
    }
}
