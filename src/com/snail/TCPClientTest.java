package com.snail;

import android.app.Activity;
import android.os.Bundle;

import java.util.logging.Logger;


/**
 * Created by artfable on 22.03.14.
 * Вопреки ожиданием связаным с названием - не является адекватным тестом)
 * После введения функционала в эксплуатацию и написания модульнух тестов - немедленно удалить!
 */
public class TCPClientTest extends Activity{

    private static Logger log = Logger.getLogger(TCPClientTest.class.getCanonicalName());

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_form);
        
        TCPClientNew client = TCPClientNew.getInstance();
        client.start();
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        client.sendMessage("starosta dno");
        log.config(client.receiveMessage());
        client.sendMessage("censor!");
        client.sendMessage("starostaDno");
        log.config(client.receiveMessage());
        client.sendMessage("Boroda4MustDie!");
        log.config(client.receiveMessage());
        client.closeConnection();
    }
}
